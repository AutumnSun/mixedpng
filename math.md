# 任意背景下的彩色幻影图生成研究

## 数学描述
$$
0 \le b, g, r, \alpha \le 1
$$

$$
\forall c \in \{b, g, r\}: c_{out} = c_{in}  \alpha + c_{bg} ( 1 - \alpha) 
$$

已知图像$I_1, I_2$, 背景$BG_1, BG_2$均为$(w, h, 3)$。
找到图像$O: (w, h, 3)$和透明度$A: (w, h, 1)$， 使
$$
I_1 = O * A + BG_1 * (1 - A) \\
I_2 = O * A + BG_2 * (1 - A) \\
$$

显然，通用解不存在。
为了处理无解的情况，首先对图像进行变换。
设$c = (c_r, r_g, c_b), k = (k_r, k_g, k_b)$，令$
\hat{I}_1 = c_1 + k_1 * I_1,
\hat{I}_2 = c_2 + k_2 * I_2,
$
将原问题转化为:
找到变换参数$k_1, k_2, c_1, c_2$, 图像$O: (w, h, 3)$和透明度$A: (w, h, 1)$，满足
$$
\hat{I}_1 = O * A + BG_1 * (1 - A) \\
\hat{I}_2 = O * A + BG_2 * (1 - A) \\
$$
同时，为了保证图像还原度，应当使$|c|$接近$(0, 0, 0)$, $k$接近$(1,1,1)$
$$
\text{argmin}: |c_1|^2+|c_2|^2 \\
\text{argmin}: |k_1 - (1, 1, 1)|^2+|k_2 - (1, 1, 1)|^2 \\
$$


## 像素点计算
Alpha合成（alpha compositing）公式:
$$ C_{out} = \cfrac{C_{in} \cdot \alpha_{in} + C_{bg} \alpha_{bg} ( 1 - \alpha)}{
    \alpha_{in} + \alpha_{bg} (1 - \alpha_{in})
} $$

当背景不透明($\alpha_{bg}=1$)时，令$\alpha = \alpha_{in}$得到:
$$ C_{out} = C_{in} \cdot \alpha + C_{bg} \cdot ( 1 - \alpha) $$

图像使用标准化表示
$$
0 \le b, g, r, \alpha \le 1
$$


对幻影图, 有:
$$ \left\{ \begin{array}{**lr**} 
    C_{out1} = C_{in} \cdot \alpha + C_{bg1} \cdot ( 1 - \alpha) \\
    C_{out2} = C_{in} \cdot \alpha + C_{bg2} \cdot ( 1 - \alpha) \\
\end{array}
\right. $$

此处$C_{out1}, C_{out2}, C_{bg1}, C_{bg2}$已知，需要求出$C_{in}, \alpha$
对任一像素点而言，rgb三个通道各有2个公式，共有6个公式，而未知数只有4个。
因此，rgb幻影图本身是无解的。

为了构造出可能的解，对图像2的显示效果进行放松, 要求变为:
$$ \left\{ \begin{aligned}
    C_{out1} &= C_{in} \cdot \alpha + C_{bg1} \cdot ( 1 - \alpha) \\
    \text{argmin} (C_{in}, \alpha): 
    \Delta &= \left| C_{in} \cdot \alpha + C_{bg2} \cdot ( 1 - \alpha) - C_{out2} \right|^2
\end{aligned}
\right. $$

从等式中可以得到：
$$
\begin{aligned}
C_{in} &= \cfrac{C_{out1} - C_{bg1} \cdot ( 1 - \alpha)}{\alpha} \\
       &= C_{bg1} + \cfrac{C_{out1} - C_{bg1}}{\alpha}
\end{aligned}
$$

<!-- 
$$ \left\{ \begin{aligned}
    b_{out1} &= b_{in} \cdot \alpha + b_{bg1} \cdot ( 1 - \alpha) \\
    g_{out1} &= g_{in} \cdot \alpha + g_{bg1} \cdot ( 1 - \alpha) \\
    r_{out1} &= r_{in} \cdot \alpha + r_{bg1} \cdot ( 1 - \alpha) \\
    \text{argmin} (b_{in}, g_{in}, r_{in}, \alpha): \Delta &= 
    (b_{in} \cdot \alpha + b_{bg2} \cdot ( 1 - \alpha) - b_{out2})^2 + 
    (g_{in} \cdot \alpha + g_{bg2} \cdot ( 1 - \alpha) - g_{out2})^2 + 
    (r_{in} \cdot \alpha + r_{bg2} \cdot ( 1 - \alpha) - r_{out2})^2
\end{aligned}
\right. $$

-->

带入$\Delta$，得
$$ \begin{aligned}
\Delta &= \left| C_{out1} - C_{bg1} \cdot ( 1 - \alpha) + C_{bg2} \cdot ( 1 - \alpha) - C_{out2} \right|^2 \\
&= \left| (C_{out1} - C_{out2}) - (C_{bg1} - C_{bg2}) \cdot ( 1 - \alpha) \right|^2
\end{aligned}
$$

令$D_o = C_{out1} - C_{out2}$, $D_b = C_{bg1} - C_{bg2}$, $\beta = 1 - \alpha$, 得:

$$ \begin{aligned}
\Delta &= \left| D_o - D_b \cdot \beta  \right|^2 \\
&= (b_o - b_b \cdot \beta)^2 + (g_o - g_b \cdot \beta)^2 + (r_o - r_b \cdot \beta)^2 \\
&= b_o^2 + g_o^2 + r_o^2 - 
2\cdot(b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b) \cdot \beta + 
(b_b^2 + g_b^2 + r_b^2) \cdot \beta^2
\end{aligned}
$$

带入二次函数最小值公式，可得$\beta = \cfrac{b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} = \cfrac{D_o \cdot D_b}{\left|D_b\right|^2}$时$\Delta$最小。
此时有:

$$
\begin{aligned}
\alpha &= 1 - \cfrac{b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} 
= 1 - \cfrac{D_o \cdot D_b}{\left|D_b\right|^2} \\
\Delta &= \cfrac{(b_b^2 + g_b^2 + r_b^2) \cdot (b_o^2 + g_o^2 + r_o^2) - (b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b)^2}{b_b^2 + g_b^2 + r_b^2} \\
    &= \cfrac{
        \left| D_o \right|^2 \cdot \left| D_b \right|^2  - (D_o \cdot D_b)^2
    }{
        \left| D_b \right|^2
    }
\end{aligned}
$$

设向量$D_o, D_b$之间的夹角为$\theta$, 则
$$
\begin{aligned}
\Delta &= \cfrac{
    \left| D_o \right|^2 \cdot \left| D_b \right|^2  - (D_o \cdot D_b)^2
    }{\left| D_b \right|^2} \\
    &=  \cfrac{
    \left| D_o \right|^2 \cdot \left| D_b \right|^2  - \left| D_o \right|^2 \cdot \left| D_b \right|^2 \cdot \cos^2(\theta)
    }{\left| D_b \right|^2} \\
    &= |D_o|^2 - |D_o|^2 \cdot \cos^2(\theta) \\
    &= |D_o|^2 \cdot( 1 - \cos^2(\theta))
\end{aligned}
$$

显然，通过减小$|D_o|$或者调整$D_o$和$D_b$的方向使$\cos(\theta)$接近1，就可以减小$\Delta$。

当$|D_o| = 0$，即目标颜色相同时，可以做到完美的还原，但这要求两种图片相近，与幻影图的需求并不一致。
而调整$\theta$的方案则要求

例如，对黑白背景下的灰度图，有$D_b \parallel D_o \parallel (1, 1, 1)$, 此时$\cos(\theta) = 1$, $\Delta = 0$, 可以做到完美的还原。
而对于对黑白背景下的彩色图，通过降低饱和度的方式能改善显示效果，就是因为降低饱和度的结果是减小
$C_{out}$更接近$(1,1,1)$的方向, 而当$C_{out1}, C_{out2}$均更接近$(1,1,1)$的方向时，$D_o$也会接近$(1,1,1)$的方向。
而黑白背景下$D_b \parallel (1,1,1)$，所以$\theta$将更接近0，进而使$\Delta$减小。

<!-- 

因此，任意像素点的最终结果为:
$$
\begin{aligned}
\alpha &= 1 - \cfrac{b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} \\
C &= C_{bg1} + \cfrac{C_{out1} - C_{bg1}}{\alpha}
\end{aligned}
$$

以$*$指代$b,g,r$， 式中$*_o = *_{out1} - *_{out2}, *_b = *_{bg1} - *_{bg2}$。

-->

## 预处理
上一步中得到的计算结果为
$$
\begin{aligned}
\alpha &= 1 - \cfrac{b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} \\
b &= b_{bg1} + \cfrac{b_{out1} - b_{bg1}}{\alpha} \\
g &= g_{bg1} + \cfrac{g_{out1} - g_{bg1}}{\alpha} \\
r &= r_{bg1} + \cfrac{r_{out1} - r_{bg1}}{\alpha} \\
\end{aligned}
$$

以$*$指代$b,g,r$， 式中$*_o = *_{out1} - *_{out2}, *_b = *_{bg1} - *_{bg2}$。

但是，这样计算得到的结果不一定能满足图片表达范围的限制$0 \le b, g, r, \alpha \le 1$.

例如，当$*_o > 0, *_b < 0$时, 将导致$\alpha > 1$。
又或者当$*_o > *_b > 0$时, 将导致$\alpha < 0$。

### 最简单情况: 黑白背景下的灰度图
此时$c_{bg1} = 1, c_{bg2} = 0$, 灰度图各通道颜色值一致, 均用$c$表示.

$$
\begin{aligned}
\alpha &= 1 - c_o = 1 - c_{out1} + c_{out2} \\
c &= 1 + \cfrac{c_{out1} - 1}{1 - c_{out1} + c_{out2}} \\
&= \cfrac{1 - c_{out1} + c_{out2} + c_{out1} - 1 }{1 - c_{out1} + c_{out2}} \\
&= \cfrac{c_{out2}}{1 - c_{out1} + c_{out2}}
\end{aligned}
$$

由$0 \le \alpha \le 1$，有
$$
\begin{aligned}
& 0 \le 1 - c_{out1} + c_{out2} \le 1 \\
\Rightarrow & -1 \le - c_{out1} + c_{out2} \le 0 \\
\Rightarrow	& 1 \ge c_{out1} - c_{out2} \ge 0 \\
\Rightarrow	& 0 \le c_{out1} - c_{out2} \le 1 \\
\Rightarrow	& c_{out1} \ge c_{out2},  c_{out1} \le 1 + c_{out2}
\end{aligned}
$$

因为$c_{out1} \le 1, c_{out2} \ge 0$, 因此$c_{out1} \le 1 + c_{out2}$始终满足。
类似地，可以推导出$0 \le c \le 1$始终成立。
因此，最终得到的限制条件为$$c_{out1} \ge c_{out2} $$

### 灰度背景下的灰度图

灰度图各通道颜色值一致, 均用$c$表示.
由$0 \le \alpha \le 1$，有
$$
\begin{aligned}
\alpha &= 1 - \cfrac{3 \cdot c_o \cdot c_b}{3 \cdot c_b^2} =  1 - \cfrac{c_o}{c_b}
= \cfrac{c_b - c_o}{ c_b } \\
c &= c_{bg1} + \cfrac{c_{out1} - c_{bg1}}{\alpha} 
= c_{bg1} + \cfrac{c_b \cdot (c_{out1} - c_{bg1})}{c_b - c_o} \\
&= \cfrac{1}{c_b - c_o} \cdot \left(c_{bg1} (c_b - c_o) + c_b (c_{out1} - c_{bg1}) \right)\\
&= \cfrac{1}{c_b - c_o} \cdot \left(c_{bg1} c_b - c_{bg1}c_o + c_b c_{out1} - c_b c_{bg1} \right)\\
&= \cfrac{1}{c_b - c_o} \cdot \left( c_b c_{out1} - c_{bg1}c_o \right) \\
&= \cfrac{1}{c_b - c_o} \cdot \left( c_{out1} (c_{bg1}-c_{bg2}) - c_{bg1}( c_{out1} - c_{out2}) \right) \\
&= \cfrac{1}{c_b - c_o} \cdot \left( c_{bg1} c_{out2}-c_{out1} c_{bg2} \right) \\
&= \cfrac{c_{bg1} c_{out2}-c_{out1} c_{bg2}}{c_b - c_o}
\end{aligned}
$$

设背景1的亮度更高, 则有$c_b \gt 0$.
由$0 \le \alpha \le 1$，有
$$
\begin{aligned}
& 0 \le \cfrac{c_b - c_o}{ c_b } \le 1 \\
\Rightarrow & 0 \le c_b - c_o \le c_b \\
\Rightarrow	& 0 \le c_o \le c_b \\
\Rightarrow	& 0 \le c_{out1} - c_{out2} \le c_b \\
\Rightarrow	& c_{out1} \ge c_{out2},  c_{out1} \le c_b + c_{out2}
\end{aligned}
$$
即要求图像1的亮度高于图像2, 并且低于背景亮度差加上图像2的亮度。

由$0 \le c$，有

$$
\begin{aligned}
& 0 \le \cfrac{c_{bg1} c_{out2}-c_{out1} c_{bg2}}{c_b - c_o}  \\
\Rightarrow &  0 \le c_{bg1} c_{out2}-c_{out1} c_{bg2} \\
\Rightarrow &  c_{bg1} c_{out2} \le c_{out1} c_{bg2} \\
\Rightarrow &  c_{out1} \ge c_{out2} \cdot \frac{c_{bg1}}{c_{bg2}} \\
\end{aligned}
$$

已知$c_{out1} \ge c_{out2}, c_{bg1} \ge c_{bg2} $


### 任意两种颜色背景下的灰度图
此时为了实现完美效果，需要将灰度图进行修改以保证$D_o \parallel D_b$。

### 黑白背景下的彩色图
此时$c_{bg1} = 1, c_{bg2} = 0, c_b = c_{bg1} - c_{bg2} = 1$
有$$
\begin{aligned}
\alpha &= 1 - \cfrac{b_o + g_o + r_o}{3} \\
& = 1 - mean(b_o, g_o, r_o)
\end{aligned}
$$

各颜色分量分别为:

$$
\begin{aligned}
c &= 1 + \cfrac{c_{out1} - 1}{1 - mean(b_o, g_o, r_o)} \\
&= \cfrac{1 - mean(b_o, g_o, r_o) + c_{out1} - 1}{1 - mean(b_o, g_o, r_o)} \\
&= \cfrac{ c_{out1} - mean(b_o, g_o, r_o) }{1 - mean(b_o, g_o, r_o)} \\
&= \cfrac{ c_{out1} - mean(b_o, g_o, r_o) }{1 - mean(b_o, g_o, r_o)} \\
\end{aligned}
$$

约束条件为:
$$
0 \le mean(b_o, g_o, r_o) \le c_{out1} \le 1 \\
b_o + g_o + r_o \le 3 \cdot c_{out1} \\
b_{out1} + g_{out1} + r_{out1} - b_{out2} - g_{out2} - r_{out2} \le 3 \cdot c_{out1} \\
$$


### 亮度递减情况
当$255 \ge *_{bg1} \ge *_{out1} \ge *_{out2} \ge *_{bg2} \ge 0 $时，有:
$$
*_b = *_{bg1} - *_{bg2} \ge *_o = *_{out1} - *_{out2} \ge 0
$$
由$0 \le \alpha \le 1$，有
$$
    0 \le \cfrac{b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} \le 1
$$

要求
$$
D_o \cdot D_b \ge 0 \\
D_o \cdot D_b \le |D_o| \cdot |D_b|
$$

### 灰度背景下的彩色图

### 任意背景下的彩色图



## 更新

预处理亮度范围可以进行扩大
只需保证对应像素亮度差小于背景亮度差即可

求得$\alpha = 1 - \cfrac{b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} $

若$\max(\alpha) \gt 1$，需要调整$c_{out1}, c_{out2}$使$\max(\alpha) = 1$.
若$\min(\alpha) \lt 0$，需要调整$c_{out1}, c_{out2}$使$\min(\alpha) = 0$.

设目标为找到参数$A_1, A_2, B_1, B_2 \in \mathbf{R}$, 使
$$
c'_{out1} = A_1 + B_1 \cdot c_{out1} \\
c'_{out2} = A_2 + B_2 \cdot c_{out2} \\
\max(\alpha') = 1 \\
\min(\alpha') = 0 \\
$$

设$A = A_1 - A_2 $。
为简单起见, 令$B_1 = B_2 = B$, 
则有:
$$
c'_o = c'_{out1} - c'_{out2} = A_1 - A_2 + (B_1 c_{out1} - B_2 c_{out2}) 
= A + B \cdot c_o
$$
然后得:
$$
\alpha' = 1 - \cfrac{b'_o \cdot b_b + g'_o \cdot g_b + r'_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} \\
 = 1 - \cfrac{(A + B b_o) b_b + (A + B g_o) g_b + (A + B r_o) r_b}{b_b^2 + g_b^2 + r_b^2} \\
 = 1 - \cfrac{A(b_b+g_b+r_b) + B (b_o b_b + g_o g_b + r_o r_b)}{b_b^2 + g_b^2 + r_b^2} \\
 = 1 - A \cfrac{b_b+g_b+r_b}{b_b^2 + g_b^2 + r_b^2}  + B \cfrac{b_o b_b + g_o g_b + r_o r_b}{b_b^2 + g_b^2 + r_b^2} \\
 = 1 - A \cfrac{b_b+g_b+r_b}{b_b^2 + g_b^2 + r_b^2}  + B (1-\alpha) \\
$$
记$R = \cfrac{b_b+g_b+r_b}{b_b^2 + g_b^2 + r_b^2}, \beta = 1 - \alpha$，分别带入原最小值和最大值所在位置.

$$
1 - A \cdot R_{atmax}  + B \beta_{atmax} = \text{MAX} \\
1 - A \cdot R_{atmin}  + B \beta_{atmin} = \text{MIN} \\
$$

求解二元一次方程, 可得
$$
B = -\cfrac{(\text{MIN} - 1)*R_{atmax} - \text{MAX}*R_{atmin} + R_{atmin}}{(\alpha_{atmin} - 1)*R_{atmax} - \alpha_{atmax}*R_{atmin}  + R_{atmin}} \\
A = -(B*\alpha_{atmax} - B + \text{MAX} - 1)/R_{atmax}
$$


为保证$0\le c \le 1$，有
$$
0 \le A_1 + B c_{out1} \le 1 \\
0 \le A_2 + B c_{out2} \le 1 \\
A = A_1 - A_2
$$
为保证图片正常显示，应要求$B \gt 0$，此时有
$$
A_1 \ge - B c_{out1.min}  \\
A_2 \ge - B c_{out2.min}  \\
A_1 \le 1 - B c_{out1.max} \\
A_2 \le 1 - B c_{out2.max} \\
A = A_1 - A_2 \\
A_1 = A + A_2 \ge A - B c_{out2.min} \\
A_1 = A + A_2 \le A + 1 - B c_{out2.max} \\
A_2 = A - A_1 \le A + B c_{out1.min} \\
A_2 = A - A_1 \ge A - 1 + B c_{out2.max} \\
$$

令$A_1$取最小值, 可得

得
$$
\cfrac{b'_o \cdot b_b + g'_o \cdot g_b + r'_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} = 1 - \cfrac{\alpha - \min(\alpha)}{\max(\alpha)-\min(\alpha)} = \cfrac{\max(\alpha) - \alpha}{\max(\alpha)-\min(\alpha)}
$$

此式可改写为
$$
b'_o \cdot b_b + g'_o \cdot g_b + r'_o \cdot r_b = C_1 + C_2 \cdot (b_o \cdot b_b + g_o \cdot g_b + r_o \cdot r_b)
$$


一般地
$$
\alpha' = 1 - \cfrac{b'_o \cdot b_b + g'_o \cdot g_b + r'_o \cdot r_b}{b_b^2 + g_b^2 + r_b^2} \\
 = 1 - \cfrac{
 \sum\limits_{c=b, g, r} (A + B_1 c_{out1} - B_2 c_{out2}) c_b
 }{b_b^2 + g_b^2 + r_b^2} \\
  = 1 - \cfrac{
  A(b_b+g_b+r_b)+
 \sum\limits_{c=b, g, r} (B_1 c_{out1} - B_2 c_{out2}) c_b
 }{b_b^2 + g_b^2 + r_b^2} \\
  = 1 - \cfrac{
  A(b_b+g_b+r_b)+
 \sum\limits_{c=b, g, r} (B_1 c_{out1} - B_2 c_{out2}) c_b
 }{b_b^2 + g_b^2 + r_b^2} \\
  = 1 - A \cfrac{b_b+g_b+r_b}{b_b^2 + g_b^2 + r_b^2}  + \cfrac{1}{b_b^2 + g_b^2 + r_b^2} 
  \sum\limits_{c=b, g, r} (B_1 c_{out1} - B_2 c_{out2}) c_b \\
$$
分别带入$\alpha$的原最大和最小值点，可得
$$
\left. 1 - (A_1 - A_2) \cfrac{b_b+g_b+r_b}{b_b^2 + g_b^2 + r_b^2}  + \cfrac{1}{b_b^2 + g_b^2 + r_b^2} 
  \sum\limits_{c=b, g, r} (B_1 c_{out1} - B_2 c_{out2}) c_b \right|_{atmax} = 1\\
\left. 1 - (A_1 - A_2) \cfrac{b_b+g_b+r_b}{b_b^2 + g_b^2 + r_b^2}  + \cfrac{1}{b_b^2 + g_b^2 + r_b^2}
  \sum\limits_{c=b, g, r} (B_1 c_{out1} - B_2 c_{out2}) c_b \right|_{atmin} = 0 \\
  \text{argmin}: (B_1 - 1)^2 + (B_2 - 1)^2 \\
  \text{argmin}: A_1^2 + A_2^2 \\
$$
